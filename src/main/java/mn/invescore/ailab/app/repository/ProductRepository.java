package mn.invescore.ailab.app.repository;

import mn.invescore.ailab.app.model.Product;
import mn.invescore.ailab.base.model.BaseModel;
import mn.invescore.ailab.base.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProductRepository extends BaseRepository <Product>{

}
