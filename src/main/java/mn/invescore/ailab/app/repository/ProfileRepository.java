package mn.invescore.ailab.app.repository;

import mn.invescore.ailab.app.model.Profile;
import mn.invescore.ailab.base.repository.BaseRepository;

public interface ProfileRepository extends BaseRepository<Profile> {
}
