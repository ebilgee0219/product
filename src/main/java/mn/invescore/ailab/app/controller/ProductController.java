package mn.invescore.ailab.app.controller;

import java.util.List;
import mn.invescore.ailab.app.model.Product;
import mn.invescore.ailab.app.service.ProductService;
import mn.invescore.ailab.base.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController  {
    private final ProductService productService;
    private static final Logger logger= LoggerFactory.getLogger(ProductController.class);
    public ProductController(ProductService productService){
        this.productService=productService;
    }
    @GetMapping("products")
    public ResponseEntity<List<Product>> getProducts(){
        logger.info("onGet");
      return ResponseEntity.ok(productService.getProducts());
    };

    @PostMapping("products")
    public ResponseEntity<Product> createProduct(@RequestBody Product product){
        logger.info("onPost");
        logger.info(product.toString());
        return ResponseEntity.ok( productService.save(product));
    }
    @PutMapping("products")
    public ResponseEntity<Product> updateProduct(@RequestBody Product product){
        Product oldProduct=productService.getProductById(product.getId());
        oldProduct.setName(product.getName());
        oldProduct.setCategory(product.getCategory());
        oldProduct.setCode(product.getCode());
        oldProduct.setState(product.getState());
        oldProduct.setValut(product.getValut());
        oldProduct.setType(product.getType());
        return ResponseEntity.ok(oldProduct);
    }
}
