package mn.invescore.ailab.app.service;

import java.util.List;
import mn.invescore.ailab.app.model.Product;
import mn.invescore.ailab.app.repository.ProductRepository;
import mn.invescore.ailab.base.repository.BaseRepository;
import mn.invescore.ailab.base.service.BaseServiceAbstractClass;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
@Service
public class ProductService extends BaseServiceAbstractClass<Product> {

    private final ProductRepository productRepository;
    public ProductService(ProductRepository productRepository){
        this.productRepository=productRepository;
    }
    public List<Product> getProducts(){
        return productRepository.findAll();
    }
    public Product save(Product product){
        try{
            return productRepository.save(product);
        }catch (Exception ex){
             throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Мэдээллийг хадгалахад алдаа гарлаа");
        }
    }

    @Override
    public Page<Product> findByQ(String q, Pageable pageable) {
        return null;
    }


    public Product getProductById(Long id){
        return productRepository.findById(id).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.BAD_REQUEST,"Тохирох id олдсонгүй."));
    }

    @Override
    public BaseRepository getRepository() {
        return this.productRepository;
    }


}
