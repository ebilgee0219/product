package mn.invescore.ailab.app.model;

import lombok.*;
import mn.invescore.ailab.base.model.BaseModel;

import javax.persistence.*;

@Entity
@Table(name = "profile")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile extends BaseModel {
    private String valut;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private Product product;
}
