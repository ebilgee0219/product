package mn.invescore.ailab.app.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import mn.invescore.ailab.base.model.BaseModel;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseModel {

    @Column(name = "code", unique = true)
    private int code;
    @Column(name = "name",length = 25)
    private String name;
    @Column(name = "type",length = 25)
    private String type;
    @Column(name = "category",length = 25)
    private String category;
    @Column(name = "valut")
    private String valut;
    @Column(name = "state")
    private String state;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
    List<Profile> profileList;

}
